# MasGlobal - Java hands-on test

This service exposes REST resources for hands-on test.

## To run the web app use maven in the root folder of the project

```
$ mvn spring-boot:run
```

## How to Test

Run unit tests with Maven:

```
$ mvn clean test
```

After the WEBAPP starts up, go to front folder and then open index.html on a browser
to start searching the employee by their id.







