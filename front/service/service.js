function callService(id) {
    return new Promise((resolve,reject) => {
        var httpRequest = new XMLHttpRequest();
        var url = "http://localhost:8080/employees/"+id;        
        httpRequest.open("GET",url,false);
        httpRequest.send();        
        resolve(httpRequest.responseText);
        
    });    
}