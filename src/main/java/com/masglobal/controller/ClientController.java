package com.masglobal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.masglobal.model.Employee;
import com.masglobal.service.ClientService;

//Use Spring controller to map the endpoints to be exposed by the WEBAPP 
@Controller
@RequestMapping("/employees")
public class ClientController {
	
	private ClientService clientService;
	
	@Autowired
	public ClientController(ClientService clientService) {
		this.clientService = clientService;
	}

	@GetMapping
	public ResponseEntity<List<Employee>> findAllEmployees() {
		final List<Employee> employees = clientService.getAllEmployees();
		return ResponseEntity.ok(employees);
	}
	
	@GetMapping("/{employeeId}")
	public ResponseEntity<Employee> findEmployeeById(@PathVariable final Integer employeeId) throws Exception {
		final Employee employee = clientService.getEmployeeById(employeeId);
		return ResponseEntity.ok(employee);
	}

}
