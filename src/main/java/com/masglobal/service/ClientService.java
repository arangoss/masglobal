package com.masglobal.service;

import java.util.List;
import com.masglobal.model.Employee;

public interface ClientService {
	
	Employee getEmployeeById(Integer employeeId) throws Exception;
	
	List<Employee> getAllEmployees();

}
