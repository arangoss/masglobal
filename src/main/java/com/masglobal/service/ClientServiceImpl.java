package com.masglobal.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.masglobal.factory.EmployeeFactory;
import com.masglobal.model.Employee;
import com.masglobal.model.EmployeeDTO;
import com.masglobal.repository.ClientRepository;

//used spring service to map the method to handle the business logic for the WEBAPP
@Service
public class ClientServiceImpl implements ClientService {
	
	private ClientRepository clientRepository;
	private EmployeeFactory employeeFactory = new EmployeeFactory();
	
	public ClientServiceImpl(ClientRepository clientRepository) {
		this.clientRepository = clientRepository;
	}

	/**
	 * Java doc for getEmployeeById for specifying the space/time complexity.
	 * As we use a List Structure to operate, it is an Array, we have a average search 0(n) according to https://www.bigocheatsheet.com/
	 * in this case we just filter and search from it so its fair, and there is no removal or insertion on the Array so it the only 
	 * heavy operation in this method so for time complexity its fair when operating in large amount of data.
	 */
	@Override
	public Employee getEmployeeById(Integer employeeId) {
		ResponseEntity<List<EmployeeDTO>> response = clientRepository.getAllEmployees();
		List<EmployeeDTO> employees = response.getBody();
		EmployeeDTO employee =  employees.stream()
				.filter(emp -> employeeId.equals(emp.getId()))
				.findAny()
				.orElse(null);
		Employee definedEmployee = employee != null ? employeeFactory.getEmployee(employee) : null;
		return definedEmployee;
	}

	@Override
	public List<Employee> getAllEmployees() {
		List<Employee> employeeList = new ArrayList<Employee>();
		ResponseEntity<List<EmployeeDTO>> response = clientRepository.getAllEmployees();
		List<EmployeeDTO> employees = response.getBody();
		employees.stream().forEach(employee -> {
			Employee definedEmployee = employeeFactory.getEmployee(employee);
			employeeList.add(definedEmployee);
		});
		return employeeList;
	}

}
