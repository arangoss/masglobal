package com.masglobal.model;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

//used lombok lib to create getter and setters and constructor for class
//, and builder for the class.
@Data
@Builder
@EqualsAndHashCode(callSuper = false)
public class HourlyEmployee extends Employee {
	
	private Integer id;
	private String name;
	private String contractTypeName;
	private Integer roleId;
	private String roleName;
	private String roleDescription;	
	
	private Double hourlySalary;

	@Override
	public double getAnnualSalary() {
		//120 * HourlySalary * 12		
		return 120 * hourlySalary * 12;
	}

}
