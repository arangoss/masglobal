package com.masglobal.model;

//Main abstract class to map the possible employee type to get the annual salary.
public abstract class Employee {
	
	abstract double getAnnualSalary();

}
