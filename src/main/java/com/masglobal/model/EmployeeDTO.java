package com.masglobal.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

//used lombok lib to create getter and setters and contructor for class
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
@AllArgsConstructor
@NoArgsConstructor
public class EmployeeDTO {
	
	private Integer id;
	private String name;
	private String contractTypeName;
	private Integer roleId;
	private String roleName;
	private String roleDescription;	
	private Double hourlySalary;
	private Double monthlySalary;

}
