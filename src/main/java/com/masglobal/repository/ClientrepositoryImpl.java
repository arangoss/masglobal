package com.masglobal.repository;

import java.util.List;

import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.RestTemplate;
import com.masglobal.model.EmployeeDTO;

//used spring repository to map the method to fetch the data from masglobal API 
@Repository
public class ClientrepositoryImpl implements ClientRepository {
	
	private RestTemplate restTemplate;
	
	public ClientrepositoryImpl(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	public ResponseEntity<List<EmployeeDTO>> getAllEmployees(){				
		return getEmployeesFromAPI();
	}
	
	public ResponseEntity<List<EmployeeDTO>> getEmployeeById(String employeeId){
		return getEmployeesFromAPI();
	}	
	
	/**
	 * method to get the employees from masglobal API
	 * @return the response entity to be mapped
	 */
	private ResponseEntity<List<EmployeeDTO>> getEmployeesFromAPI() {
		ResponseEntity<List<EmployeeDTO>> response =  restTemplate.
				exchange("http://masglobaltestapi.azurewebsites.net/api/employees", 
						HttpMethod.GET, null, new ParameterizedTypeReference<List<EmployeeDTO>>() {
        });
		return response;
	}

}
