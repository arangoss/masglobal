package com.masglobal.repository;

import java.util.List;
import org.springframework.http.ResponseEntity;
import com.masglobal.model.EmployeeDTO;

public interface ClientRepository {	
	
	public ResponseEntity<List<EmployeeDTO>> getAllEmployees();
	
	public ResponseEntity<List<EmployeeDTO>> getEmployeeById(String employeeId);	 

}
