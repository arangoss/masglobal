package com.masglobal.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;
//I Used rest template as a spring tool for making the masglobal request. 
@Configuration
public class RestTemplateConfiguration {
	
	@Bean
	public RestTemplate restTemplate() {
	    return new RestTemplate();
	}

}
