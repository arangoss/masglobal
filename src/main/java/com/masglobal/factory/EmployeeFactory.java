package com.masglobal.factory;

import com.masglobal.model.Employee;
import com.masglobal.model.EmployeeDTO;
import com.masglobal.model.HourlyEmployee;
import com.masglobal.model.MonthlyEmployee;

//Employee factory to make the concrete classes based on the contract type 
public class EmployeeFactory {
	
	private final static String HOURLYSALARYEMPLOYEE = "HourlySalaryEmployee";
	private final static String MONTHLYSALARYEMPLOYEE = "MonthlySalaryEmployee";
	
	public Employee getEmployee(EmployeeDTO employee) {
		switch (employee.getContractTypeName()) {
		case HOURLYSALARYEMPLOYEE:
			return HourlyEmployee.builder()
					.id(employee.getId())
					.name(employee.getName())
					.contractTypeName(employee.getContractTypeName())
					.roleId(employee.getRoleId())
					.roleName(employee.getRoleName())
					.roleDescription(employee.getRoleDescription())
					.hourlySalary(employee.getHourlySalary())
					.build();

		case MONTHLYSALARYEMPLOYEE:
			return MonthlyEmployee.builder()
					.id(employee.getId())
					.name(employee.getName())
					.contractTypeName(employee.getContractTypeName())
					.roleId(employee.getRoleId())
					.roleName(employee.getRoleName())
					.roleDescription(employee.getRoleDescription())
					.monthlySalary(employee.getMonthlySalary())
					.build();

		}
		return null;	
	}

}
