package com.masglobal.service;

import java.util.ArrayList;
import java.util.List;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import com.masglobal.model.Employee;
import com.masglobal.model.EmployeeDTO;
import com.masglobal.repository.ClientRepository;

@RunWith(MockitoJUnitRunner.class)
public class ClientServiceTest {
	
	@InjectMocks
	private ClientServiceImpl clientService;
	
	@Mock
	private ClientRepository clientRepository;
	
	static List<EmployeeDTO> employeeList = new ArrayList<>();
	static ResponseEntity<List<EmployeeDTO>> response;
	
	@BeforeAll
	public static void setUp() {


		EmployeeDTO hourlyEmployee = new EmployeeDTO(1,"Andres","MonthlySalaryEmployee",
				1,"Administrator","Admin",60000D, null);
		employeeList.add(hourlyEmployee);
		
		EmployeeDTO monthlyEmployee = new EmployeeDTO(1,"Andres","MonthlySalaryEmployee",
				1,"Administrator","Admin",null, 60000D);
	
		employeeList.add(monthlyEmployee);
		
		response = ResponseEntity.ok(employeeList);
	  }
	
	@Test
	public void getEmployeeByIdTest() {
		MockitoAnnotations.initMocks(this);		
		Mockito.when(clientRepository.getAllEmployees()).thenReturn(response);
		List<Employee> employeeList = clientService.getAllEmployees();
		Assert.notNull(employeeList, "Employee list must no be null");
		assertThat(employeeList.size(), is(2));
	}
	
	

}
